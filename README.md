# GEN3 Control

This package contains source code and configuration files to control the Kinova Gen3 7DOF with ROS. It provides access to the Kortex low-level API for joint position control.  It is based in a previous implementation for controlling a [DENSO VS050](https://github.com/jcolan/vs050_control)

## How to use it

Follow the instructions in https://github.com/jcolan/vs050_control

## Publications
This code has been used as the core controller for the following publications. For further references please follow the links:

* Colan, Jacinto, et al. "[A Concurrent Framework for Constrained Inverse Kinematics of Minimally Invasive Surgical Robots ](https://www.mdpi.com/1424-8220/23/6/3328)." Sensors v.23, no.6, p.3328, 2023.
* Fozilov, Khusniddin, et al. "[Toward Autonomous Robotic Minimally Invasive Surgery: A Hybrid Framework Combining Task-Motion Planning and Dynamic Behavior Trees ](https://ieeexplore.ieee.org/document/10230219)." IEEE Access v.11, pp.91206-91224, 2023.