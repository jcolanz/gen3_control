/******************************************************************************
# gen3_example.cpp: Gen3 Example                                            # #
# Copyright (c) 2021                                                          #
# Hasegawa Laboratory at Nagoya University                                    #
#                                                                             #
# Redistribution and use in source and binary forms, with or without          #
# modification, are permitted provided that the following conditions are met: #
#                                                                             #
#     - Redistributions of source code must retain the above copyright        #
#       notice, this list of conditions and the following disclaimer.         #
#     - Redistributions in binary form must reproduce the above copyright     #
#       notice, this list of conditions and the following disclaimer in the   #
#       documentation and/or other materials provided with the distribution.  #
#     - Neither the name of the Hasegawa Laboratory nor the                   #
#       names of its contributors may be used to endorse or promote products  #
#       derived from this software without specific prior written permission. #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License LGPL as         #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                #
# GNU Lesser General Public License LGPL for more details.                    #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public            #
# License LGPL along with this program.                                       #
# If not, see <http://www.gnu.org/licenses/>.                                 #
#                                                                             #
# #############################################################################
#                                                                             #
#   Author: Jacinto E. Colan, email: colan@robo.mein.nagoya-u.ac.jp           #
#                                                                             #
# ###########################################################################*/

// C
#include <signal.h>

// ROS
#include <ros/ros.h>
#include <std_msgs/Int32MultiArray.h>
#include <geometry_msgs/Pose.h>

// Eigen
#include <Eigen/Dense>

// Eigen conversions
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>

// Actions
#include <actionlib/client/simple_action_client.h>
#include "gen3_control/FollowJointCmdAction.h"
#include "gen3_control/FollowToolCmdAction.h"

#include <gen3_control/gen3_status_code.h>
#include <zivid_camera/CalibrationCapture.h>

using namespace Eigen;
using namespace gen3_control;

typedef actionlib::SimpleActionClient<FollowJointCmdAction>
    FollowJointCmdClient;
typedef actionlib::SimpleActionClient<FollowToolCmdAction> FollowToolCmdClient;

class Gen3CalibrationZivid
{
public:
  Gen3CalibrationZivid(ros::NodeHandle &node_handle, std::string prefix)
      : ac_joint_cmd_(prefix + "/FollowJoint", true),
        ac_tool_cmd_(prefix + "/FollowTool", true)
  {
    nh_ = node_handle;
    seq_state_ = 0;
    act_state_ = -1;
    n_joints_ = 7;
    robot_status_ = R_UNINITIALIZED;

    sub_robot_state_ =
        nh_.subscribe(prefix + "/arm_state", 1,
                      &Gen3CalibrationZivid::updateRobotStateCb, this);

    if (!nh_.getParam("initial_pos_x", initial_pos_[0]))
    {
      initial_pos_[0] = 320;
    }

    if (!nh_.getParam("initial_pos_y", initial_pos_[1]))
    {
      initial_pos_[1] = 440;
    }

    if (!nh_.getParam("initial_pos_z", initial_pos_[2]))
    {
      initial_pos_[2] = 100;
    }
    zivid_client_ = nh_.serviceClient<zivid_camera::CalibrationCapture>(
        "/zivid/calibration_cmd");
  }
  ~Gen3CalibrationZivid() {}

  // Callbacks
  void updateRobotStateCb(const std_msgs::Int32MultiArray::ConstPtr &msg)
  {
    robot_status_ = msg->data[0];
  }

  void actFollowJointCmdDoneCb(const actionlib::SimpleClientGoalState &state,
                               const FollowJointCmdResultConstPtr &result)
  {
    bool res = result->succeed;
    if (res == true)
    {
      ROS_INFO_STREAM("Joint target reached");
      if (seq_state_ < 4)
        seq_state_ = seq_state_ + 1;
      else
        seq_state_ = -1;
    }
    else
    {
      ROS_WARN_STREAM("Joint target aborted ");
    }
  }

  void actFollowToolCmdDoneCb(const actionlib::SimpleClientGoalState &state,
                              const FollowToolCmdResultConstPtr &result)
  {
    bool res = result->succeed;
    if (res == true)
    {
      ROS_INFO_STREAM("Tool target reached");
      if (seq_state_ < 10)
      {
        zivid_srv_.request.command = "pose";

        zivid_srv_.request.pose.clear();

        zivid_srv_.request.pose.push_back(ori_cmd_(0, 0));
        zivid_srv_.request.pose.push_back(ori_cmd_(0, 1));
        zivid_srv_.request.pose.push_back(ori_cmd_(0, 2));
        zivid_srv_.request.pose.push_back(pos_cmd_(0));
        zivid_srv_.request.pose.push_back(ori_cmd_(1, 0));
        zivid_srv_.request.pose.push_back(ori_cmd_(1, 1));
        zivid_srv_.request.pose.push_back(ori_cmd_(1, 2));
        zivid_srv_.request.pose.push_back(pos_cmd_(1));
        zivid_srv_.request.pose.push_back(ori_cmd_(2, 0));
        zivid_srv_.request.pose.push_back(ori_cmd_(2, 1));
        zivid_srv_.request.pose.push_back(ori_cmd_(2, 2));
        zivid_srv_.request.pose.push_back(pos_cmd_(2));
        zivid_srv_.request.pose.push_back(0);
        zivid_srv_.request.pose.push_back(0);
        zivid_srv_.request.pose.push_back(0);
        zivid_srv_.request.pose.push_back(1);
      }
      else
      {
        zivid_srv_.request.command = "calibrate";
        zivid_srv_.request.pose;
        seq_state_ = -1;
      }
      ROS_INFO("Calling Zivid RecordPose Service");
      ros::Duration(1).sleep();

      if (zivid_client_.call(zivid_srv_))
      {
        ROS_INFO("Pose added");
      }
      else
      {
        ROS_ERROR("Failed to call service ");
      }

      // ros::ServiceClient client = nh_.serviceClient<zivid_camera::RecordPose>(
      //     "/zivid_camera/record_pose");
      ros::Duration(8).sleep();
      seq_state_ = seq_state_ + 1;
    }
    else
    {
      ROS_WARN_STREAM("Tool target aborted ");
    }
  }

  void sendJointGoal(VectorXd joint_seq)
  {
    FollowJointCmdGoal joint_goal;
    joint_goal.joint_target.name.resize(n_joints_);
    joint_goal.joint_target.position.resize(n_joints_);
    joint_goal.joint_target.velocity.resize(n_joints_);
    joint_goal.joint_target.effort.resize(n_joints_);

    VectorXd::Map(&joint_goal.joint_target.position[0],
                  joint_goal.joint_target.position.size()) = joint_seq;
    VectorXd::Map(&joint_goal.joint_target.velocity[0],
                  joint_goal.joint_target.velocity.size()) =
        VectorXd::Zero(n_joints_);
    VectorXd::Map(&joint_goal.joint_target.effort[0],
                  joint_goal.joint_target.effort.size()) =
        VectorXd::Zero(n_joints_);

    ROS_INFO_STREAM("Waiting for action server to start");
    ac_joint_cmd_.waitForServer();
    ROS_INFO_STREAM("Action server started. Sending goal");
    ac_joint_cmd_.sendGoal(
        joint_goal,
        boost::bind(&Gen3CalibrationZivid::actFollowJointCmdDoneCb, this, _1,
                    _2),
        FollowJointCmdClient::SimpleActiveCallback(),
        FollowJointCmdClient::SimpleFeedbackCallback());
  }

  void sendToolGoal(Vector3d pd, Matrix3d Rd)
  {
    FollowToolCmdGoal tool_goal;
    Quaterniond qd(Rd);

    Affine3d Td;
    Td = Affine3d::Identity();
    Td.linear() = Rd;
    Td.translation() = pd;

    tf::poseEigenToMsg(Td, tool_goal.tool_pose);

    ROS_INFO_STREAM("Waiting for action server to start");
    ac_tool_cmd_.waitForServer();
    ROS_INFO_STREAM("Action server started. Sending goal");
    ac_tool_cmd_.sendGoal(
        tool_goal,
        boost::bind(&Gen3CalibrationZivid::actFollowToolCmdDoneCb, this, _1,
                    _2),
        FollowToolCmdClient::SimpleActiveCallback(),
        FollowToolCmdClient::SimpleFeedbackCallback());
  }

  int sendJointSeq()
  {
    VectorXd joint_seq;
    joint_seq.resize(7);

    if (robot_status_ >= R_SLAVE and seq_state_ != act_state_)
    {
      switch (seq_state_)
      {
      case 0:
      {
        act_state_ = seq_state_;
        // joint_seq << 10, 10, 10, 10, 10, 10;
        joint_seq << -30, 10, 60, 1, 1, 1, 1;
        joint_seq = joint_seq * M_PI / 180;
        ROS_WARN_STREAM("Sending Goal-0:");
        ROS_WARN_STREAM("J:" << joint_seq.transpose());
        sendJointGoal(joint_seq);
        break;
      }
      case 1:
      {
        act_state_ = seq_state_;
        // joint_seq << 30, 30, 30, 30, 30, 30;
        joint_seq << 0, 15, 75, 10, 10, 10, 10;
        joint_seq = joint_seq * M_PI / 180;

        ROS_WARN_STREAM("Sending Goal-1:");
        ROS_WARN_STREAM("J:" << joint_seq.transpose());
        sendJointGoal(joint_seq);
        break;
      }
      case 2:
      {
        act_state_ = seq_state_;
        // joint_seq << 60, 60, 60, 60, 60, 60;
        joint_seq << 30, 30, 90, 30, 30, 30, 30;
        joint_seq = joint_seq * M_PI / 180;

        ROS_WARN_STREAM("Sending Goal-2:");
        ROS_WARN_STREAM("J:" << joint_seq.transpose());
        sendJointGoal(joint_seq);
        break;
      }
      case 3:
      {
        act_state_ = seq_state_;
        // joint_seq << 30, 30, 30, 30, 30, 30;
        joint_seq << 0, 15, 75, 10, 10, 10, 10;
        joint_seq = joint_seq * M_PI / 180;

        ROS_WARN_STREAM("Sending Goal-3:");
        ROS_WARN_STREAM("J:" << joint_seq.transpose());
        sendJointGoal(joint_seq);
        break;
      }
      case 4:
      {
        act_state_ = seq_state_;
        // joint_seq << 10, 10, 10, 10, 10, 10;
        joint_seq << -30, 10, 60, 1, 1, 1, 1;
        joint_seq = joint_seq * M_PI / 180;

        ROS_WARN_STREAM("Sending Goal-4:");
        ROS_WARN_STREAM("J:" << joint_seq.transpose());
        sendJointGoal(joint_seq);
        break;
      }
      case -1:
        return -1;
      }
    }

    return 0;
  }

  void reset_demo_state() { seq_state_ = 0; }

  Matrix3d rotationAroundAxisX(double theta)
  {
    Matrix3d RotX;

    RotX << 1, 0, 0, 0, cos(theta), -sin(theta), 0, sin(theta), cos(theta);
    return RotX;
  }

  Matrix3d rotationAroundAxisY(double theta)
  {
    Matrix3d RotY;

    RotY << cos(theta), 0, sin(theta), 0, 1, 0, -sin(theta), 0, cos(theta);
    return RotY;
  }

  Matrix3d rotationAroundAxisZ(double theta)
  {
    Matrix3d RotZ;

    RotZ << cos(theta), -sin(theta), 0, sin(theta), cos(theta), 0, 0, 0, 1;
    return RotZ;
  }

  int sendToolSeq()
  {
    Matrix3d R_seq;
    Vector3d p_seq;
    Vector3d ref_p_center, end_p;
    Matrix3d ref_R_center, end_R;

    Matrix3d Rot45X;
    Rot45X << 1, 0, 0, 0, cos((15) * (M_PI / 180)), -sin((15) * (M_PI / 180)),
        0, sin((15) * (M_PI / 180)), cos((15) * (M_PI / 180));

    // ref_p_center << 320, 440, 100;
    ref_p_center = initial_pos_;
    ref_R_center << 0, 1, 0,
        1, 0, 0,
        0, 0, -1;

    end_p << 0.45151397585869, 0.10176993906498, 0.36810570955276;
    end_p = end_p * 1000;
    end_R << 0.81444591283798, 0.32981723546982, -0.47738710045815,
        0.58023941516876, -0.46262049674988, 0.67030173540115,
        0.00022801756858826, -0.8229233622551, -0.56815230846405;

    double step_d = 15.0;

    if (robot_status_ >= R_SLAVE and seq_state_ != act_state_)
    {
      switch (seq_state_)
      {
      case 0:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center;

        ROS_WARN_STREAM("Sending Goal-0");
        ROS_WARN_STREAM("p:\n"
                        << p_seq.transpose());
        ROS_WARN_STREAM("R:\n"
                        << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 1:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(-1.1, 0.1, 0);

        ROS_WARN_STREAM("Sending Goal-1");
        ROS_WARN_STREAM("p:\n"
                        << p_seq.transpose());
        ROS_WARN_STREAM("R:\n"
                        << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 2:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(-1.2, -1.2, 0);

        ROS_WARN_STREAM("Sending Goal-2");
        ROS_WARN_STREAM("p:" << p_seq.transpose());
        ROS_WARN_STREAM("R:" << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 3:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(0.1, -1.15, 1.05);
        ROS_WARN_STREAM("Sending Goal-3");
        ROS_WARN_STREAM("p:" << p_seq.transpose());
        ROS_WARN_STREAM("R:" << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 4:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(1.05, -1.1, 1.1);
        ROS_WARN_STREAM("Sending Goal-4");
        ROS_WARN_STREAM("p:" << p_seq.transpose());
        ROS_WARN_STREAM("R:" << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 5:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(1.15, 0.05, 1.15);

        ROS_WARN_STREAM("Sending Goal-5");
        ROS_WARN_STREAM("p:" << p_seq.transpose());
        ROS_WARN_STREAM("R:" << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 6:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;
        p_seq << ref_p_center + step_d * Vector3d(1, 1, 0);
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;

        ROS_WARN_STREAM("Sending Goal-6");
        ROS_WARN_STREAM("p:" << p_seq.transpose());
        ROS_WARN_STREAM("R:" << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 7:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(0, 1, -1);
        ROS_WARN_STREAM("Sending Goal-7");
        ROS_WARN_STREAM("p:\n"
                        << p_seq.transpose());
        ROS_WARN_STREAM("R:\n"
                        << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 8:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;
        R_seq << rotationAroundAxisZ((15) * (M_PI / 180)) *
                     rotationAroundAxisY(-(35) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(-1, 1, -1);
        ROS_WARN_STREAM("Sending Goal-8");
        ROS_WARN_STREAM("p:" << p_seq.transpose());
        ROS_WARN_STREAM("R:" << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 9:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;

        R_seq << rotationAroundAxisZ((25) * (M_PI / 180)) *
                     rotationAroundAxisY(-(30) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(-1, 0, -1);
        ROS_WARN_STREAM("Sending Goal-9");
        ROS_WARN_STREAM("p:" << p_seq.transpose());
        ROS_WARN_STREAM("R:" << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      case 10:
      {
        act_state_ = seq_state_;
        // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;

        R_seq << rotationAroundAxisZ((25) * (M_PI / 180)) *
                     rotationAroundAxisY(-(30) * (M_PI / 180)) * ref_R_center;
        p_seq << ref_p_center + step_d * Vector3d(-1, -0.5, -1);
        ROS_WARN_STREAM("Sending Goal-10");
        ROS_WARN_STREAM("p:" << p_seq.transpose());
        ROS_WARN_STREAM("R:" << R_seq);
        sendToolGoal(p_seq, R_seq);
        break;
      }
      // case 11:
      // {
      //   act_state_ = seq_state_;
      //   // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
      //   // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;

      //   R_seq << rotationAroundAxisZ((25) * (M_PI / 180)) *
      //                rotationAroundAxisY(-(30) * (M_PI / 180)) * ref_R_center;
      //   p_seq << ref_p_center + step_d * Vector3d(0, -0.5, 1);
      //   ROS_WARN_STREAM("Sending Goal-11");
      //   ROS_WARN_STREAM("p:" << p_seq.transpose());
      //   ROS_WARN_STREAM("R:" << R_seq);
      //   sendToolGoal(p_seq, R_seq);
      //   break;
      // }
      // case 12:
      // {
      //   act_state_ = seq_state_;
      //   // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
      //   // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;

      //   R_seq << rotationAroundAxisZ((25) * (M_PI / 180)) *
      //                rotationAroundAxisY(-(30) * (M_PI / 180)) * ref_R_center;
      //   p_seq << ref_p_center + step_d * Vector3d(0.5, 0, -1);
      //   ROS_WARN_STREAM("Sending Goal-12");
      //   ROS_WARN_STREAM("p:" << p_seq.transpose());
      //   ROS_WARN_STREAM("R:" << R_seq);
      //   sendToolGoal(p_seq, R_seq);
      //   break;
      // }
      // case 13:
      // {
      //   act_state_ = seq_state_;
      //   // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
      //   // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;

      //   R_seq << rotationAroundAxisZ((25) * (M_PI / 180)) *
      //                rotationAroundAxisY(-(30) * (M_PI / 180)) * ref_R_center;
      //   p_seq << ref_p_center + step_d * Vector3d(1, 0.5, 0);
      //   ROS_WARN_STREAM("Sending Goal-13");
      //   ROS_WARN_STREAM("p:" << p_seq.transpose());
      //   ROS_WARN_STREAM("R:" << R_seq);
      //   sendToolGoal(p_seq, R_seq);
      //   break;
      // }
      // case 14:
      // {
      //   act_state_ = seq_state_;
      //   // R_seq << 1, 0, 0, 0, 1, 0, 0, 0, 1;
      //   // R_seq << -1, 0, 0, 0, 1, 0, 0, 0, -1;
      //   R_seq << rotationAroundAxisY(-(30) * (M_PI / 180)) * ref_R_center;
      //   p_seq << ref_p_center;
      //   ROS_WARN_STREAM("Sending Goal-14");
      //   ROS_WARN_STREAM("p:" << p_seq.transpose());
      //   ROS_WARN_STREAM("R:" << R_seq);
      //   sendToolGoal(p_seq, R_seq);
      //   break;
      // }
      case -1:
        return -1;
      }
      pos_cmd_ = p_seq;
      ori_cmd_ = R_seq;
    }

    return 0;
  }

private:
  ros::NodeHandle nh_;

  // Subscriber
  ros::Subscriber sub_robot_state_;
  // Service
  ros::ServiceClient zivid_client_;

  //   Action clients
  FollowJointCmdClient ac_joint_cmd_;
  FollowToolCmdClient ac_tool_cmd_;

  zivid_camera::CalibrationCapture zivid_srv_;

  int seq_state_;
  int act_state_;
  int n_joints_;
  int robot_status_;
  Vector3d pos_cmd_;
  Matrix3d ori_cmd_;
  Vector3d initial_pos_;
};

bool kill_this_process = false;

void SigIntHandler(int signal)
{
  kill_this_process = true;
  ROS_INFO_STREAM("SHUTDOWN SIGNAL RECEIVED");
}

/**
 * Main function: Initialize the commun
 */
int main(int argc, char **argv)
{

  int demo = 0;
  // Initialize ROS
  ros::init(argc, argv, "gen3_example");
  ros::NodeHandle node_handle;

  std::string prefix;

  // Updating Parameters
  if (!node_handle.getParam("prefix", prefix))
  {
    prefix = "";
  }

  ros::Rate loop_rate(100);

  signal(SIGINT, SigIntHandler);
  Gen3CalibrationZivid dd(node_handle, prefix);

  while (ros::ok)
  {
    if (kill_this_process)
    {
      ROS_INFO_STREAM("Killing ROS");
      break;
    }

    // if (demo == 0)
    // {
    //   int res_joint = dd.sendJointSeq();
    //   if (res_joint == -1)
    //   {
    //     demo = 1;
    //     dd.reset_demo_state();
    //     ROS_INFO_STREAM("Gen3 JOINT example sequence finished.");
    //   }
    // }
    // else
    // {
    int res_tool = dd.sendToolSeq();
    if (res_tool == -1)
    {
      ROS_INFO_STREAM("Gen3 arm TOOL calibration sequence finished.");
      break;
    }
    // }

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}