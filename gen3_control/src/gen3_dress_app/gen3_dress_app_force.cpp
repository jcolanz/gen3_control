/******************************************************************************
# gen3_example.cpp: Gen3 Example                                            # #
# Copyright (c) 2021                                                          #
# Hasegawa Laboratory at Nagoya University                                    #
#                                                                             #
# Redistribution and use in source and binary forms, with or without          #
# modification, are permitted provided that the following conditions are met: #
#                                                                             #
#     - Redistributions of source code must retain the above copyright        #
#       notice, this list of conditions and the following disclaimer.         #
#     - Redistributions in binary form must reproduce the above copyright     #
#       notice, this list of conditions and the following disclaimer in the   #
#       documentation and/or other materials provided with the distribution.  #
#     - Neither the name of the Hasegawa Laboratory nor the                   #
#       names of its contributors may be used to endorse or promote products  #
#       derived from this software without specific prior written permission. #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License LGPL as         #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                #
# GNU Lesser General Public License LGPL for more details.                    #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public            #
# License LGPL along with this program.                                       #
# If not, see <http://www.gnu.org/licenses/>.                                 #
#                                                                             #
# #############################################################################
#                                                                             #
#   Author: Jacinto E. Colan, email: colan@robo.mein.nagoya-u.ac.jp           #
#                                                                             #
# ###########################################################################*/

// C
#include <signal.h>

// ROS
#include <ros/ros.h>
#include <std_msgs/Int32MultiArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Wrench.h>

// Eigen
#include <Eigen/Dense>

// Eigen conversions
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>

// Actions
#include <actionlib/client/simple_action_client.h>
#include "gen3_control/FollowJointCmdAction.h"
#include "gen3_control/FollowToolCmdAction.h"

#include <gen3_control/gen3_status_code.h>

using namespace Eigen;
using namespace gen3_control;

typedef actionlib::SimpleActionClient<FollowJointCmdAction> FollowJointCmdClient;
typedef actionlib::SimpleActionClient<FollowToolCmdAction> FollowToolCmdClient;

class Gen3Demo
{
public:
  Gen3Demo(ros::NodeHandle &node_handle, std::string prefix)
      : ac_joint_cmd_(prefix + "/FollowJoint", true),
        ac_tool_cmd_(prefix + "/FollowTool", true)
  {
    nh_ = node_handle;
    seq_state_ = 0;
    act_state_ = -1;
    n_joints_ = 7;
    robot_status_ = R_UNINITIALIZED;

    robot_target_ << 300, -600, 150;

    sub_robot_state_ =
        nh_.subscribe(prefix + "/arm_state", 1, &Gen3Demo::updateRobotStateCb, this);
    sub_robot_target_ =
        nh_.subscribe(prefix + "/robotarm_to_grasp", 1, &Gen3Demo::updateRobotTargetCb, this);
    sub_force_ =
        nh_.subscribe("/unit0/ati_mini40/6axis_force", 1, &Gen3Demo::updateForceCb, this);
  }
  ~Gen3Demo() {}

  // Callbacks
  void updateRobotStateCb(const std_msgs::Int32MultiArray::ConstPtr &msg)
  {
    robot_status_ = msg->data[0];
  }

  void updateRobotTargetCb(const geometry_msgs::Pose::ConstPtr &msg)
  {
    robot_target_[0] = msg->position.x * 1000;
    robot_target_[1] = msg->position.y * 1000;
    robot_target_[2] = msg->position.z * 1000;

    // ROS_INFO_STREAM("New target " << robot_target_.transpose());
    // robot_target_ = VectorXd::Map(&msg->data[0], 3);
  }

  void updateForceCb(const geometry_msgs::Wrench::ConstPtr &msg)
  {
    force_sensor_[0] = msg->force.x;
    force_sensor_[1] = msg->force.y;
    force_sensor_[2] = msg->force.z;

    if (force_sensor_[1] > 2)
    {
      touch_check_ = true;
    }

    ROS_INFO_STREAM("New force " << force_sensor_.transpose());
    // robot_target_ = VectorXd::Map(&msg->data[0], 3);
  }

  void actFollowJointCmdDoneCb(const actionlib::SimpleClientGoalState &state,
                               const FollowJointCmdResultConstPtr &result)
  {
    bool res = result->result;
    if (res == EXIT_SUCCESS)
    {
      ROS_INFO_STREAM("Joint target reached");
      if (seq_state_ == 1)
      {
        act_state_ = 0;

        if (status_check_ > 2)
        {
          point = point + 10;

          if (touch_check_)
          {
            seq_state_ = seq_state_ + 1;
          }
          else if (point == 100)
          {
            point = 0;
            seq_state_ = 0;
            status_check_ = 0;
            touch_check_ = false;
          }
        }
      }
      else if (seq_state_ == 0 or seq_state_ == 2)
      {
        seq_state_ = seq_state_ + 1;
      }
    }
    else
    {
      ROS_WARN_STREAM("Joint target aborted ");
    }
  }

  void actFollowToolCmdDoneCb(const actionlib::SimpleClientGoalState &state,
                              const FollowToolCmdResultConstPtr &result)
  {
    bool res = result->result;
    if (res == EXIT_SUCCESS)
    {
      ROS_INFO_STREAM("Tool target reached");
      ROS_INFO_STREAM(status_check_);
      ROS_INFO_STREAM(touch_check_);
      ROS_INFO_STREAM(point);

      if (seq_state_ == 1)
      {
        act_state_ = 0;

        if (status_check_ > 2)
        {
          point = point + 10;

          if (touch_check_)
          {
            seq_state_ = seq_state_ + 1;
          }
          else if (point == 100)
          {
            point = 0;
            seq_state_ = 0;
            status_check_ = 0;
            touch_check_ = false;
          }
        }
      }
      else if (seq_state_ == 0 or seq_state_ == 2)
      {
        seq_state_ = seq_state_ + 1;
      }
    }
    else
    {
      ROS_WARN_STREAM("Tool target aborted ");
    }
  }

  void sendJointGoal(VectorXd joint_seq)
  {
    FollowJointCmdGoal joint_goal;
    joint_goal.joint_pos.name.resize(n_joints_);
    joint_goal.joint_pos.position.resize(n_joints_);
    joint_goal.joint_pos.velocity.resize(n_joints_);
    joint_goal.joint_pos.effort.resize(n_joints_);

    VectorXd::Map(&joint_goal.joint_pos.position[0],
                  joint_goal.joint_pos.position.size()) = joint_seq;
    VectorXd::Map(&joint_goal.joint_pos.velocity[0],
                  joint_goal.joint_pos.velocity.size()) = VectorXd::Zero(n_joints_);
    VectorXd::Map(&joint_goal.joint_pos.effort[0], joint_goal.joint_pos.effort.size()) =
        VectorXd::Zero(n_joints_);

    ROS_INFO_STREAM("Waiting for action server to start");
    ac_joint_cmd_.waitForServer();
    ROS_INFO_STREAM("Action server started. Sending goal");
    ac_joint_cmd_.sendGoal(joint_goal,
                           boost::bind(&Gen3Demo::actFollowJointCmdDoneCb, this, _1, _2),
                           FollowJointCmdClient::SimpleActiveCallback(),
                           FollowJointCmdClient::SimpleFeedbackCallback());
  }

  void sendToolGoal(Vector3d pd, Matrix3d Rd)
  {
    FollowToolCmdGoal tool_goal;
    Quaterniond qd(Rd);

    Affine3d Td;
    Td = Affine3d::Identity();
    Td.linear() = Rd;
    Td.translation() = pd;

    tf::poseEigenToMsg(Td, tool_goal.tool_pose);

    ROS_INFO_STREAM("Waiting for action server to start");
    ac_tool_cmd_.waitForServer();
    ROS_INFO_STREAM("Action server started. Sending goal");
    ac_tool_cmd_.sendGoal(tool_goal,
                          boost::bind(&Gen3Demo::actFollowToolCmdDoneCb, this, _1, _2),
                          FollowToolCmdClient::SimpleActiveCallback(),
                          FollowToolCmdClient::SimpleFeedbackCallback());
  }

  int sendToolTarget()
  {
    Matrix3d R_target; // Rot. matrix target (EEF) w.r.t. Robot Base
    Vector3d p_target; // Position target (EEF) w.r.t. Robot Base

    if (robot_status_ >= R_SLAVE and seq_state_ != act_state_)
    // if (robot_status_ >= R_SLAVE)
    {
      act_state_ = seq_state_;

      // ROS_INFO_STREAM(seq_state_);

      if (seq_state_ == 1)
      {
        ROS_INFO_STREAM(before_position_[1]);
        ROS_INFO_STREAM(robot_target_[1]);

        if (before_position_[0] == robot_target_[0] and before_position_[1] == robot_target_[1] and before_position_[2] == robot_target_[2])
        {
          if (status_check_ > 2)
          {

            R_target << 0, 1, 0,
                1, 0, 0,
                0, 0, -1;

            Matrix3d R_tmp; // Rotation around X-axis
            R_tmp << 1, 0, 0,
                0, cos(-30 * M_PI / 180), -sin(-30 * M_PI / 180),
                0, sin(-30 * M_PI / 180), cos(-30 * M_PI / 180);

            R_target = R_tmp * R_target;

            // p_target << 250, -100, 500;
            p_target << 400, -400, 120;
            p_target = robot_target_;
            before_position_ = robot_target_;
            p_target[1] = robot_target_[1] - point;
            ROS_WARN_STREAM("Sending Target with seq_state = " << seq_state_);
            ROS_WARN_STREAM("p:" << p_target.transpose());
            ROS_WARN_STREAM("R:" << R_target);
            sendToolGoal(p_target, R_target);
          }
          else
          {
            status_check_ = status_check_ + 1;
            touch_check_ = false;

            R_target << 0, 1, 0,
                1, 0, 0,
                0, 0, -1;

            Matrix3d R_tmp; // Rotation around X-axis
            R_tmp << 1, 0, 0,
                0, cos(-30 * M_PI / 180), -sin(-30 * M_PI / 180),
                0, sin(-30 * M_PI / 180), cos(-30 * M_PI / 180);

            R_target = R_tmp * R_target;

            // p_target << 250, -100, 500;
            p_target << 400, -400, 120;
            p_target = robot_target_;
            before_position_ = robot_target_;
            ROS_WARN_STREAM("Sending Target with seq_state = " << seq_state_);
            ROS_WARN_STREAM("p:" << p_target.transpose());
            ROS_WARN_STREAM("R:" << R_target);
            sendToolGoal(p_target, R_target);
          }
        }
        else
        {
          status_check_ = 0;
          point = 0;
          touch_check_ = false;

          R_target << 0, 1, 0,
              1, 0, 0,
              0, 0, -1;

          Matrix3d R_tmp; // Rotation around X-axis
          R_tmp << 1, 0, 0,
              0, cos(-30 * M_PI / 180), -sin(-30 * M_PI / 180),
              0, sin(-30 * M_PI / 180), cos(-30 * M_PI / 180);

          R_target = R_tmp * R_target;

          // p_target << 250, -100, 500;
          p_target << 400, -400, 120;
          p_target = robot_target_;
          before_position_ = robot_target_;
          ROS_WARN_STREAM("Sending Target with seq_state = " << seq_state_);
          ROS_WARN_STREAM("p:" << p_target.transpose());
          ROS_WARN_STREAM("R:" << R_target);
          sendToolGoal(p_target, R_target);
        }
      }
      else if (seq_state_ == 2)
      {
        R_target << 0, 1, 0,
            1, 0, 0,
            0, 0, -1;

        Matrix3d R_tmp; // Rotation around X-axis
        R_tmp << 1, 0, 0,
            0, cos(-30 * M_PI / 180), -sin(-30 * M_PI / 180),
            0, sin(-30 * M_PI / 180), cos(-30 * M_PI / 180);

        R_target = R_tmp * R_target;

        // p_target << 250, -100, 500;
        p_target = robot_target_;
        before_position_ = robot_target_;
        p_target[2] = robot_target_[2] - 500;
        ROS_WARN_STREAM("Sending Target");
        ROS_WARN_STREAM("p:" << p_target.transpose());
        ROS_WARN_STREAM("R:" << R_target);
        sendToolGoal(p_target, R_target);
      }
      else if (seq_state_ == 0)
      {
        R_target << 0, 1, 0,
            1, 0, 0,
            0, 0, -1;

        Matrix3d R_tmp; // Rotation around X-axis
        R_tmp << 1, 0, 0,
            0, cos(-30 * M_PI / 180), -sin(-30 * M_PI / 180),
            0, sin(-30 * M_PI / 180), cos(-30 * M_PI / 180);

        R_target = R_tmp * R_target;

        // p_target << 250, -100, 500;
        // p_target << 400, -200, 300;
        p_target = robot_target_;
        before_position_ = robot_target_;
        p_target[1] = robot_target_[1] + 100;
        p_target[2] = robot_target_[2] + 50;
        ROS_WARN_STREAM("Sending Target");
        ROS_WARN_STREAM("p:" << p_target.transpose());
        ROS_WARN_STREAM("R:" << R_target);
        sendToolGoal(p_target, R_target);
      }
    }
    return 0;
  }

private:
  ros::NodeHandle nh_;

  // Subscriber
  ros::Subscriber sub_robot_state_;
  ros::Subscriber sub_robot_target_, sub_force_;

  //   Action clients
  FollowJointCmdClient ac_joint_cmd_;
  FollowToolCmdClient ac_tool_cmd_;

  int seq_state_;
  int act_state_;
  int n_joints_;
  int robot_status_;
  int status_check_ = 0;
  int point = 0;
  bool touch_check_ = false;
  Vector3d robot_target_;
  Vector3d before_position_;
  Vector3d force_sensor_;
};

bool kill_this_process = false;

void SigIntHandler(int signal)
{
  kill_this_process = true;
  ROS_INFO_STREAM("SHUTDOWN SIGNAL RECEIVED");
}

/**
 * Main function: Initialize the commun
 */
int main(int argc, char **argv)
{

  int demo = 0;

  // Initialize ROS
  ros::init(argc, argv, "gen3_example");
  ros::NodeHandle node_handle;

  std::string prefix;

  // Updating Parameters
  if (!node_handle.getParam("prefix", prefix))
  {
    prefix = "";
  }

  ros::Rate loop_rate(2); // Hz

  signal(SIGINT, SigIntHandler);
  Gen3Demo dd(node_handle, prefix);

  while (ros::ok)
  {
    if (kill_this_process)
    {
      ROS_INFO_STREAM("Killing ROS");
      break;
    }

    // ROS_INFO("Sending target");
    int res_tool = dd.sendToolTarget();
    if (res_tool == -1)
    {
      ROS_INFO_STREAM("Gen3 arm TOOL target reached.");
      break;
    }

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}